robokobo
========

Tools for automating and scaling Kobo eReader interaction.

Setup
=====

The Robokobo tools are intended to be installed on your Kobo eReader device.
This ensures that you have access both to the tools and their configuration data
no matter what computer you plug your Kobo into.

To get started, plug your Kobo into a computer and authorize USB access. Then
clone this repository into the device's root folder. E.g. on a Mac:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /Volumes/KOBOeReader
git clone https://gitlab.com/dgc/robokobo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may wish to add Robokobo's `bin/` folder to your `$PATH` for ease of access.

Robokobo will work as-is, but you can get more value from it by creating a
configuration file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /Volumes/KOBOeReader/robokobo
cp robokobo.conf.example robokobo.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Then edit it to taste.

Usage
=====

Kobo stores all its metadata and configuration in the `/.kobo` folder of the
device's storage. Most book and device metadata is in a sqlite3 database file.
Robokobo's main function is to give you tools for manipulating this database.

There are several primary tools here:

bin/kbackup
-----------

`kbackup` copies `/.kobo` to `/backups` (also on the device itself), and
performs a sqlite dump into `/backups/sqldump`. This is just a safety measure;
if anything damages your database the device will think it needs to be set up
from scratch again. This enables you to recover it.

bin/kmkshelf
------------

This is a simple shell script that creates "shelves" in your Kobo for organizing
books. This can be done in the Kobo UI, but it can be done much faster with
`kmkshelf`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: kmkshelf [-c | --category] shelfname ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each `shelfname` provided becomes a shelf. If you use the `-c` or `--category`
option, shelf names are prefixed with a bullet to differentiate them. This is
rooted in my own organizational habits, but should probably be a more
configurable behavior.

bin/ksql
--------

This is a simple wrapper around `sqlite3`. Its stdin is applied to the Kobo
database. Use it in conjunction with `kshelve`.

bin/kshelve
-----------

`kshelve` is a slightly more complex tool for managing Kobo bookshelves and for
filing books among them. Currently, out of paranoia, it never directly
manipulates the database; rather, it produces SQL that can be applied to the
database. Thus a typical pattern is:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
kshelve [options ...] | ksql
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`kshelve` can work with any book, but some actions are only supported with ePub
files.

Here's the full usage:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: kshelve [-h] [-d] [-c] [-r] [-s SHELF] [-l] [-t] [-f] [-p PREFIX]
               files [files ...]

positional arguments:
  files                 book files to shelve

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           debug things
  -c, --category        shelf name is a category
  -r, --recursive       shelve files found recursively
  -s SHELF, --shelf SHELF
                        shelf name to shelve into
  -l, --list-tags       list tags for file(s)
  -t, --tags            shelve by tags
  -f, --fingerprint     fingerprint book
  -p PREFIX, --prefix PREFIX
                        match only tags with prefix
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Books must reside on the Kobo device, but can be located anywhere. By default
books are stored in the root folder directly but you can impose your own
organizational strategies if you wish.

### \--shelf

`--shelf` is used to explicitly file a book, given by the path to its file, into
a named bookshelf. This works with any file format because it only adds an entry
to the database's shelf table with exactly the arguments you provide.

Example: `kshelve -s foo path/to/book.pdf`

### \--category

`--category`, as with `mkshelf`, denotes that a shelf name is a topic category
and should be prefixed with "• ".

### \--recursive

Rather than listing all books individually, you can give a folder name.
`--recursive` instructs that any folders should be descended to find book files.

### \--list-tags

`kshelve` can extract certain metadata from ePub files. `--list-tags` tells it
to find a book's tags (perhaps set up by Calibre, perhaps not) and display them.

### \--tags

Because we can parse tags from ePub files, we can also automatically shelve
books into shelves matching tag names. The shelves will be created if necessary.
*This is perhaps dangerous on its own, because any tag, whether you like it or
not, will be created as a shelf.* Thus we have:

### \--prefix

By using `--prefix` you can limit `--tag` autoshelving to tags that begin with
the given prefix. The prefix itself is removed. So `kshelve -t -p local.
dictionary.epub` will autoshelve dictionary.epub into the 'Reference' shelf (not
automatic title-casing), provided that `dictionary.epub` is tagged
`local.reference`. The `local.` is removed.

./refresh.sh
------------

This is a handy batch script to simplify incremental processing. Use it to
perform a backup and then autoshelve books that have been added recently.
**Creating your own **`robokobo.conf`** file is strongly advised.**

Contributing
============

Patches welcome.

License
=======

Robokobo is free in all senses. See `LICENSE`.
