#!/bin/sh

cd $(dirname "$0")
PATH=$(pwd)/bin:"$PATH"
if [ -f ./robokobo.conf ]; then
	. ./robokobo.conf
else
	. ./robokobo.conf.example
fi

kbackup
kshelve -r -t ${TAG_PREFIX:+-p "${TAG_PREFIX}"} ../${BOOKS_FOLDER} | ksql
